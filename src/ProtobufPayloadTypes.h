#ifndef PROTOBUF_PAYLOAD_TYPES_H__
#define PROTOBUF_PAYLOAD_TYPES_H__

#include <mama/types.h>
#include <google/protobuf/repeated_field.h>
#include <MamaPacket.pb.h>

// Additional types in the same namespace as Packet
namespace ProtobufPayload
{
    typedef google::protobuf::RepeatedPtrField<Field> FieldVector;

    struct Opaque
    {
        const void* data;
        mama_size_t size;
        Opaque() : data(NULL), size(0) {}
        Opaque(const void* data, mama_size_t size) : data(data), size(size) {}
    };
}

#endif
