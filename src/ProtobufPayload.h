#ifndef PROTOBUF_PAYLOAD_H__
#define PROTOBUF_PAYLOAD_H__

#include <mama/types.h>
#include <payloadbridge.h>

#define PROTOBUF_SUBJ_FIELD "__subj"

// C wrapper interface
extern "C" {

// Bridge functions
// ****************

MAMAExpBridgeDLL
extern mama_status
protobufPayload_createImpl(mamaPayloadBridge* result, 
                           char*              identifier);

MAMAExpBridgeDLL
extern mama_status 
protobufPayload_destroyImpl(mamaPayloadBridge mamaPayloadBridge);

extern mamaPayloadType
protobufPayload_getType(void);

// General functions
// *****************

extern mama_status
protobufPayload_create(msgPayload* msg);

extern mama_status
protobufPayload_createForTemplate(msgPayload*       msg,
                                  mamaPayloadBridge bridge,
                                  mama_u32_t        templateId);

extern mama_status
protobufPayload_copy(const msgPayload msg,
                     msgPayload*      copy);

extern mama_status
protobufPayload_clear(msgPayload msg);

extern mama_status
protobufPayload_destroy(msgPayload msg);

extern mama_status
protobufPayload_setParent(msgPayload    msg,
                          const mamaMsg parent);

extern mama_status
protobufPayload_getByteSize(const msgPayload msg,
                            mama_size_t*     size);

extern mama_status
protobufPayload_getNumFields(const msgPayload msg,
                             mama_size_t*     numFields);

extern mama_status
protobufPayload_getSendSubject(const msgPayload msg,
                               const char **    subject);

extern const char*
protobufPayload_toString(const msgPayload msg);

extern mama_status
protobufPayload_iterateFields(const msgPayload  msg,
                              const mamaMsg     parent,
                              mamaMsgField      field,
                              mamaMsgIteratorCb cb,
                              void*             closure);

extern mama_status
protobufPayload_getFieldAsString(const msgPayload msg,
                                 const char*      name,
                                 mama_fid_t       fid,
                                 char*            buffer,
                                 mama_size_t      len);

extern mama_status
protobufPayload_serialize(const msgPayload payload,
                          const void**     buffer,
                          mama_size_t*     length);

extern mama_status
protobufPayload_unSerialize(const msgPayload payload,
                            const void**     buffer,
                            mama_size_t      length);

extern mama_status
protobufPayload_getByteBuffer(const msgPayload msg,
                              const void**     buffer,
                              mama_size_t*     length);
extern mama_status
protobufPayload_setByteBuffer(const msgPayload  msg,
                              mamaPayloadBridge bridge,
                              const void*       buffer,
                              mama_size_t       length);

extern mama_status
protobufPayload_createFromByteBuffer(msgPayload*       msg,
                                     mamaPayloadBridge bridge,
                                     const void*       buffer,
                                     mama_size_t       bufferLength);
extern mama_status
protobufPayload_apply(msgPayload       dest,
                      const msgPayload src);

extern mama_status
protobufPayload_getNativeMsg(const msgPayload msg,
                             void**           nativeMsg);

// Add functions
// *************

extern mama_status
protobufPayload_addBool(msgPayload  msg,
                        const char* name,
                        mama_fid_t  fid,
                        mama_bool_t value);

extern mama_status
protobufPayload_addChar(msgPayload  msg,
                        const char* name,
                        mama_fid_t  fid,
                        char        value);

extern mama_status
protobufPayload_addI8(msgPayload  msg,
                      const char* name,
                      mama_fid_t  fid,
                      mama_i8_t   value);

extern mama_status
protobufPayload_addU8(msgPayload  msg,
                      const char* name,
                      mama_fid_t  fid,
                      mama_u8_t   value);

extern mama_status
protobufPayload_addI16(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_i16_t  value);

extern mama_status
protobufPayload_addU16(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_u16_t  value);

extern mama_status
protobufPayload_addI32(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_i32_t  value);

extern mama_status
protobufPayload_addU32(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_u32_t  value);

extern mama_status
protobufPayload_addI64(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_i64_t  value);

extern mama_status
protobufPayload_addU64(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_u64_t  value);

extern mama_status
protobufPayload_addF32(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_f32_t  value);

extern mama_status
protobufPayload_addF64(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_f64_t  value);

extern mama_status
protobufPayload_addString(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          const char* value);
extern mama_status
protobufPayload_addOpaque(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          const void* value,
                          mama_size_t size);

extern mama_status
protobufPayload_addDateTime(msgPayload         msg,
                            const char*        name,
                            mama_fid_t         fid,
                            const mamaDateTime value);

extern mama_status
protobufPayload_addPrice(msgPayload      msg,
                         const char*     name,
                         mama_fid_t      fid,
                         const mamaPrice value);

extern mama_status
protobufPayload_addMsg(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       msgPayload  value);

// Add vector functions
// ********************

extern mama_status
protobufPayload_addVectorBool(msgPayload        msg,
                              const char*       name,
                              mama_fid_t        fid,
                              const mama_bool_t value[],
                              mama_size_t       size);

extern mama_status
protobufPayload_addVectorChar(msgPayload  msg,
                              const char* name,
                              mama_fid_t  fid,
                              const char  value[],
                              mama_size_t size);

extern mama_status
protobufPayload_addVectorI8(msgPayload      msg,
                            const char*     name,
                            mama_fid_t      fid,
                            const mama_i8_t value[],
                            mama_size_t     size);

extern mama_status
protobufPayload_addVectorU8(msgPayload      msg,
                            const char*     name,
                            mama_fid_t      fid,
                            const mama_u8_t value[],
                            mama_size_t     size);

extern mama_status
protobufPayload_addVectorI16(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_i16_t value[],
                             mama_size_t      size);

extern mama_status
protobufPayload_addVectorU16(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_u16_t value[],
                             mama_size_t      size);

extern mama_status
protobufPayload_addVectorI32(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_i32_t value[],
                             mama_size_t      size);

extern mama_status
protobufPayload_addVectorU32(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_u32_t value[],
                             mama_size_t      size);

extern mama_status
protobufPayload_addVectorI64(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_i64_t value[],
                             mama_size_t      size);

extern mama_status
protobufPayload_addVectorU64(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_u64_t value[],
                             mama_size_t      size);

extern mama_status
protobufPayload_addVectorF32(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_f32_t value[],
                             mama_size_t      size);

extern mama_status
protobufPayload_addVectorF64(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_f64_t value[],
                             mama_size_t      size);

extern mama_status
protobufPayload_addVectorString(msgPayload  msg,
                                const char* name,
                                mama_fid_t  fid,
                                const char* value[],
                                mama_size_t size);

extern mama_status
protobufPayload_addVectorMsg(msgPayload    msg,
                             const char*   name,
                             mama_fid_t    fid,
                             const mamaMsg value[],
                             mama_size_t   size);

extern mama_status
protobufPayload_addVectorDateTime(msgPayload         msg,
                                  const char*        name,
                                  mama_fid_t         fid,
                                  const mamaDateTime value[],
                                  mama_size_t        size);

extern mama_status
protobufPayload_addVectorPrice(msgPayload      msg,
                               const char*     name,
                               mama_fid_t      fid,
                               const mamaPrice value[],
                               mama_size_t     size);

// Update functions
// ****************

extern mama_status
protobufPayload_updateBool(msgPayload  msg,
                           const char* name,
                           mama_fid_t  fid,
                           mama_bool_t value);

extern mama_status
protobufPayload_updateChar(msgPayload  msg,
                           const char* name,
                           mama_fid_t  fid,
                           char        value);

extern mama_status
protobufPayload_updateU8(msgPayload  msg,
                         const char* name,
                         mama_fid_t  fid,
                         mama_u8_t   value);

extern mama_status
protobufPayload_updateI8(msgPayload  msg,
                         const char* name,
                         mama_fid_t  fid,
                         mama_i8_t   value);

extern mama_status
protobufPayload_updateI16(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_i16_t  value);

extern mama_status
protobufPayload_updateU16(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_u16_t  value);

extern mama_status
protobufPayload_updateI32(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_i32_t  value);

extern mama_status
protobufPayload_updateU32(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_u32_t  value);

extern mama_status
protobufPayload_updateI64(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_i64_t  value);

extern mama_status
protobufPayload_updateU64(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_u64_t  value);

extern mama_status
protobufPayload_updateF32(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_f32_t  value);

extern mama_status
protobufPayload_updateF64(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_f64_t  value);

extern mama_status
protobufPayload_updateString(msgPayload  msg,
                             const char* name,
                             mama_fid_t  fid,
                             const char* value);

extern mama_status
protobufPayload_updateOpaque(msgPayload  msg,
                             const char* name,
                             mama_fid_t  fid,
                             const void* data,
                             mama_size_t size);

extern mama_status
protobufPayload_updateDateTime(msgPayload         msg,
                               const char*        name,
                               mama_fid_t         fid,
                               const mamaDateTime value);
extern mama_status
protobufPayload_updatePrice(msgPayload      msg,
                            const char*     name,
                            mama_fid_t      fid,
                            const mamaPrice value);

extern mama_status
protobufPayload_updateSubMsg(msgPayload       msg,
                             const char*      fname,
                             mama_fid_t       fid,
                             const msgPayload subMsg);

// Update vector functions
// ***********************

extern mama_status
protobufPayload_updateVectorMsg(msgPayload    msg,
                                const char*   fname,
                                mama_fid_t    fid,
                                const mamaMsg value[],
                                mama_size_t   size);

extern mama_status
protobufPayload_updateVectorString(msgPayload  msg,
                                   const char* fname,
                                   mama_fid_t  fid,
                                   const char* value[],
                                   mama_size_t size);

extern mama_status
protobufPayload_updateVectorBool(msgPayload        msg,
                                 const char*       fname,
                                 mama_fid_t        fid,
                                 const mama_bool_t value[],
                                 mama_size_t       size);

extern mama_status
protobufPayload_updateVectorChar(msgPayload  msg,
                                 const char* fname,
                                 mama_fid_t  fid,
                                 const char  value[],
                                 mama_size_t size);

extern mama_status
protobufPayload_updateVectorI8(msgPayload      msg,
                               const char*     fname,
                               mama_fid_t      fid,
                               const mama_i8_t value[],
                               mama_size_t     size);

extern mama_status
protobufPayload_updateVectorU8(msgPayload      msg,
                               const char*     fname,
                               mama_fid_t      fid,
                               const mama_u8_t value[],
                               mama_size_t     size);

extern mama_status
protobufPayload_updateVectorI16(msgPayload       msg,
                                const char*      fname,
                                mama_fid_t       fid,
                                const mama_i16_t value[],
                                mama_size_t      size);

extern mama_status
protobufPayload_updateVectorU16(msgPayload       msg,
                                const char*      fname,
                                mama_fid_t       fid,
                                const mama_u16_t value[],
                                mama_size_t      size);

extern mama_status
protobufPayload_updateVectorI32(msgPayload       msg,
                                const char*      fname,
                                mama_fid_t       fid,
                                const mama_i32_t value[],
                                mama_size_t      size);

extern mama_status
protobufPayload_updateVectorU32(msgPayload       msg,
                                const char*      fname,
                                mama_fid_t       fid,
                                const mama_u32_t value[],
                                mama_size_t      size);

extern mama_status
protobufPayload_updateVectorI64(msgPayload       msg,
                                const char*      fname,
                                mama_fid_t       fid,
                                const mama_i64_t value[],
                                mama_size_t      size);

extern mama_status
protobufPayload_updateVectorU64(msgPayload       msg,
                                const char*      fname,
                                mama_fid_t       fid,
                                const mama_u64_t value[],
                                mama_size_t      size);

extern mama_status
protobufPayload_updateVectorF32(msgPayload       msg,
                                const char*      fname,
                                mama_fid_t       fid,
                                const mama_f32_t value[],
                                mama_size_t      size);

extern mama_status
protobufPayload_updateVectorF64(msgPayload       msg,
                                const char*      fname,
                                mama_fid_t       fid,
                                const mama_f64_t value[],
                                mama_size_t      size);

extern mama_status
protobufPayload_updateVectorPrice(msgPayload       msg,
                                  const char*      fname,
                                  mama_fid_t       fid,
                                  const mamaPrice* value[],
                                  mama_size_t      size);

extern mama_status
protobufPayload_updateVectorTime(msgPayload         msg,
                                 const char*        fname,
                                 mama_fid_t         fid,
                                 const mamaDateTime value[],
                                 mama_size_t        size);

// Get functions
// *************

extern mama_status
protobufPayload_getChar(const msgPayload msg,
                        const char*      name,
                        mama_fid_t       fid,
                        char*            result);

extern mama_status
protobufPayload_getBool(const msgPayload msg,
                        const char*      name,
                        mama_fid_t       fid,
                        mama_bool_t*     result);

extern mama_status
protobufPayload_getI8(const msgPayload msg,
                      const char*      name,
                      mama_fid_t       fid,
                      mama_i8_t*       result);

extern mama_status
protobufPayload_getU8(const msgPayload msg,
                      const char*      name,
                      mama_fid_t       fid,
                      mama_u8_t*       result);

extern mama_status
protobufPayload_getI16(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_i16_t*      result);

extern mama_status
protobufPayload_getU16(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_u16_t*      result);

extern mama_status
protobufPayload_getI32(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_i32_t*      result);

extern mama_status
protobufPayload_getU32(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_u32_t*      result);

extern mama_status
protobufPayload_getI64(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_i64_t*      result);

extern mama_status
protobufPayload_getU64(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_u64_t*      result);

extern mama_status
protobufPayload_getF32(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_f32_t*      result);

extern mama_status
protobufPayload_getF64(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_f64_t*      result);

extern mama_status
protobufPayload_getString(const msgPayload msg,
                          const char*      name,
                          mama_fid_t       fid,
                          const char**     result);

extern mama_status
protobufPayload_getOpaque(const msgPayload msg,
                          const char*      name,
                          mama_fid_t       fid,
                          const void**     data,
                          mama_size_t*     size);

extern mama_status
protobufPayload_getField(const msgPayload msg,
                         const char*      name,
                         mama_fid_t       fid,
                         msgFieldPayload* result);

extern mama_status
protobufPayload_getDateTime(const msgPayload msg,
                            const char*      name,
                            mama_fid_t       fid,
                            mamaDateTime     result);

extern mama_status
protobufPayload_getPrice(const msgPayload msg,
                         const char*      name,
                         mama_fid_t       fid,
                         mamaPrice        result);

extern mama_status
protobufPayload_getMsg(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       msgPayload*      result);

// Get vector functions
// ********************

extern mama_status
protobufPayload_getVectorBool(const msgPayload    msg,
                              const char*         name,
                              mama_fid_t          fid,
                              const mama_bool_t** result,
                              mama_size_t*        size);

extern mama_status
protobufPayload_getVectorChar(const msgPayload msg,
                              const char*      name,
                              mama_fid_t       fid,
                              const char**     result,
                              mama_size_t*     size);

extern mama_status
protobufPayload_getVectorI8(const msgPayload  msg,
                            const char*       name,
                            mama_fid_t        fid,
                            const mama_i8_t** result,
                            mama_size_t*      size);

extern mama_status
protobufPayload_getVectorU8(const msgPayload  msg,
                            const char*       name,
                            mama_fid_t        fid,
                            const mama_u8_t** result,
                            mama_size_t*      size);

extern mama_status
protobufPayload_getVectorI16(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_i16_t** result,
                             mama_size_t*       size);

extern mama_status
protobufPayload_getVectorU16(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_u16_t** result,
                             mama_size_t*       size);

extern mama_status
protobufPayload_getVectorI32(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_i32_t** result,
                             mama_size_t*       size);

extern mama_status
protobufPayload_getVectorU32(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_u32_t** result,
                             mama_size_t*       size);

extern mama_status
protobufPayload_getVectorI64(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_i64_t** result,
                             mama_size_t*       size);

extern mama_status
protobufPayload_getVectorU64(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_u64_t** result,
                             mama_size_t*       size);

extern mama_status
protobufPayload_getVectorF32(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_f32_t** result,
                             mama_size_t*       size);

extern mama_status
protobufPayload_getVectorF64(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_f64_t** result,
                             mama_size_t*       size);

extern mama_status
protobufPayload_getVectorString(const msgPayload msg,
                                const char*      name,
                                mama_fid_t       fid,
                                const char***    result,
                                mama_size_t*     size);

extern mama_status
protobufPayload_getVectorDateTime(const msgPayload    msg,
                                  const char*         name,
                                  mama_fid_t          fid,
                                  const mamaDateTime* result,
                                  mama_size_t*        size);

extern mama_status
protobufPayload_getVectorPrice(const msgPayload msg,
                               const char*      name,
                               mama_fid_t       fid,
                               const mamaPrice* result,
                               mama_size_t*     size);

extern mama_status
protobufPayload_getVectorMsg(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const msgPayload** result,
                             mama_size_t*       size);

// Iterator functions
// ******************

extern mama_status
protobufPayloadIter_create(msgPayloadIter* itr,
                           msgPayload      msg);

extern msgFieldPayload
protobufPayloadIter_next(msgPayloadIter  itr,
                         msgFieldPayload field,
                         msgPayload      msg);

extern mama_bool_t
protobufPayloadIter_hasNext(msgPayloadIter itr,
                            msgPayload     msg);

extern msgFieldPayload
protobufPayloadIter_begin(msgPayloadIter  itr,
                          msgFieldPayload field,
                          msgPayload      msg);

extern msgFieldPayload
protobufPayloadIter_end(msgPayloadIter itr,
                        msgPayload     msg);

extern mama_status
protobufPayloadIter_associate(msgPayloadIter itr,
                              msgPayload     msg);

extern mama_status
protobufPayloadIter_destroy(msgPayloadIter itr);

// Field general functions
// ***********************

extern mama_status
protobufFieldPayload_create(msgFieldPayload* field);

extern mama_status
protobufFieldPayload_destroy(msgFieldPayload field);

extern mama_status
protobufFieldPayload_getName(const msgFieldPayload field,
                             mamaDictionary        dict,
                             mamaFieldDescriptor   desc,
                             const char**          result);

extern mama_status
protobufFieldPayload_getFid(const msgFieldPayload field,
                            mamaDictionary        dict,
                            mamaFieldDescriptor   desc,
                            uint16_t*             result);

extern mama_status
protobufFieldPayload_getDescriptor(const msgFieldPayload field,
                                   mamaDictionary        dict,
                                   mamaFieldDescriptor*  result);

extern mama_status
protobufFieldPayload_getType(const msgFieldPayload field,
                             mamaFieldType*        result);
 
extern mama_status
protobufFieldPayload_getAsString (const msgFieldPayload field,
                                  const msgPayload      msg,
                                  char*                 buf,
                                  mama_size_t           len);

// Field update functions
// **********************

extern mama_status
protobufFieldPayload_updateBool(msgFieldPayload field,
                                msgPayload      msg,
                                mama_bool_t     value);

extern mama_status
protobufFieldPayload_updateChar(msgFieldPayload field,
                                msgPayload      msg,
                                char            value);

extern mama_status
protobufFieldPayload_updateU8(msgFieldPayload field,
                              msgPayload      msg,
                              mama_u8_t       value);

extern mama_status
protobufFieldPayload_updateI8(msgFieldPayload field,
                              msgPayload      msg,
                              mama_i8_t       value);

extern mama_status
protobufFieldPayload_updateI16(msgFieldPayload field,
                               msgPayload      msg,
                               mama_i16_t      value);

extern mama_status
protobufFieldPayload_updateU16(msgFieldPayload field,
                               msgPayload      msg,
                               mama_u16_t      value);

extern mama_status
protobufFieldPayload_updateI32(msgFieldPayload field,
                               msgPayload      msg,
                               mama_i32_t      value);

extern mama_status
protobufFieldPayload_updateU32(msgFieldPayload field,
                               msgPayload      msg,
                               mama_u32_t      value);

extern mama_status
protobufFieldPayload_updateI64(msgFieldPayload field,
                               msgPayload      msg,
                               mama_i64_t      value);

extern mama_status
protobufFieldPayload_updateU64(msgFieldPayload field,
                               msgPayload      msg,
                               mama_u64_t      value);

extern mama_status
protobufFieldPayload_updateF32(msgFieldPayload field,
                               msgPayload      msg,
                               mama_f32_t      value);

extern mama_status
protobufFieldPayload_updateF64(msgFieldPayload field,
                               msgPayload      msg,
                               mama_f64_t      value);

extern mama_status
protobufFieldPayload_updateDateTime(msgFieldPayload    field,
                                    msgPayload         msg,
                                    const mamaDateTime value);

extern mama_status
protobufFieldPayload_updatePrice(msgFieldPayload field,
                                 msgPayload      msg,
                                 const mamaPrice value);

// Field get functions
// *******************

extern mama_status
protobufFieldPayload_getBool(const msgFieldPayload field,
                             mama_bool_t*          result);

extern mama_status
protobufFieldPayload_getChar(const msgFieldPayload field,
                             char*                 result);

extern mama_status
protobufFieldPayload_getI8(const msgFieldPayload field,
                           mama_i8_t*            result);

extern mama_status
protobufFieldPayload_getU8(const msgFieldPayload field,
                           mama_u8_t*            result);

extern mama_status
protobufFieldPayload_getI16(const msgFieldPayload field,
                            mama_i16_t*           result);

extern mama_status
protobufFieldPayload_getU16(const msgFieldPayload field,
                            mama_u16_t*           result);

extern mama_status
protobufFieldPayload_getI32(const msgFieldPayload field,
                            mama_i32_t*           result);

extern mama_status
protobufFieldPayload_getU32(const msgFieldPayload field,
                            mama_u32_t*           result);

extern mama_status
protobufFieldPayload_getI64(const msgFieldPayload field,
                            mama_i64_t*           result);

extern mama_status
protobufFieldPayload_getU64(const msgFieldPayload field,
                            mama_u64_t*           result);

extern mama_status
protobufFieldPayload_getF32(const msgFieldPayload field,
                            mama_f32_t*           result);

extern mama_status
protobufFieldPayload_getF64(const msgFieldPayload field,
                            mama_f64_t*           result);

extern mama_status
protobufFieldPayload_getString(const msgFieldPayload field,
                               const char**          result);

extern mama_status
protobufFieldPayload_getOpaque(const msgFieldPayload field,
                               const void**          result,
                               mama_size_t*          size);

extern mama_status
protobufFieldPayload_getField(const msgFieldPayload field,
                              mamaMsgField*         result);

extern mama_status
protobufFieldPayload_getDateTime(const msgFieldPayload field,
                                 mamaDateTime          result);

extern mama_status
protobufFieldPayload_getPrice(const msgFieldPayload field,
                              mamaPrice             result);

extern mama_status
protobufFieldPayload_getMsg(const msgFieldPayload field,
                            msgPayload*           result);

// Field get vector functions
// **************************

extern mama_status
protobufFieldPayload_getVectorBool(const msgFieldPayload field,
                                   const mama_bool_t**   result,
                                   mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorChar(const msgFieldPayload field,
                                   const char**          result,
                                   mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorI8(const msgFieldPayload field,
                                 const mama_i8_t**     result,
                                 mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorU8(const msgFieldPayload field,
                                 const mama_u8_t**     result,
                                 mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorI16(const msgFieldPayload field,
                                  const mama_i16_t**    result,
                                  mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorU16(const msgFieldPayload field,
                                  const mama_u16_t**    result,
                                  mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorI32(const msgFieldPayload field,
                                  const mama_i32_t**    result,
                                  mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorU32(const msgFieldPayload field,
                                  const mama_u32_t**    result,
                                  mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorI64(const msgFieldPayload field,
                                  const mama_i64_t**    result,
                                  mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorU64(const msgFieldPayload field,
                                  const mama_u64_t**    result,
                                  mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorF32(const msgFieldPayload field,
                                  const mama_f32_t**    result,
                                  mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorF64(const msgFieldPayload field,
                                  const mama_f64_t**    result,
                                  mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorString(const msgFieldPayload field,
                                     const char***         result,
                                     mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorDateTime(const msgFieldPayload field,
                                       const mamaDateTime*   result,
                                       mama_size_t*           size);

extern mama_status
protobufFieldPayload_getVectorPrice(const msgFieldPayload field,
                                    const mamaPrice*      result,
                                    mama_size_t*          size);

extern mama_status
protobufFieldPayload_getVectorMsg(const msgFieldPayload field,
                                  const msgPayload**    result,
                                  mama_size_t*          size);

} // extern "C"

#endif /* PROTOBUF_PAYLOAD_H__*/
