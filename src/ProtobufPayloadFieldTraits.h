#ifndef PROTOBUF_PAYLOAD_FIELD_TRAITS_H__
#define PROTOBUF_PAYLOAD_FIELD_TRAITS_H__

#include <mama/types.h>
#include <MamaPacket.pb.h>
#include <ProtobufPayloadTypes.h>

namespace ProtobufPayload
{
    template <FieldType>
    struct FieldTraits;

    // BOOL traits
    template <>
    struct FieldTraits<BOOL>
    {
        typedef mama_bool_t MamaType;

        static void set(Field* field, MamaType value) 
        { 
            field->set_valbool(static_cast<bool>(value)); 
        }
        
        static MamaType get(const Field& field) 
        { 
            return static_cast<MamaType>(field.valbool()); 
        }
    };

    // CHAR traits
    template <>
    struct FieldTraits<CHAR>
    {
        typedef char MamaType;

        static void set(Field* field, MamaType value) 
        { 
            field->set_valchar(static_cast<uint32_t>(value)); 
        }
        
        static MamaType get(const Field& field) 
        { 
            return static_cast<MamaType>(field.valchar()); 
        }
    };

    // INT8 traits
    template <>
    struct FieldTraits<INT8>
    {
        typedef mama_i8_t MamaType;
 
        static void set(Field* field, MamaType value) 
        { 
            field->set_valint8(static_cast<int32_t>(value)); 
        }
        
        static MamaType get(const Field& field) 
        { 
            return static_cast<MamaType>(field.valint8()); 
        }
    };
 
    // UINT8 traits
    template <>
    struct FieldTraits<UINT8>
    {
        typedef mama_u8_t MamaType;
 
        static void set(Field* field, MamaType value) 
        { 
            field->set_valint8(static_cast<uint32_t>(value)); 
        }
        
        static MamaType get(const Field& field) 
        { 
            return static_cast<MamaType>(field.valint8()); 
        }
    };
    
    // INT16 traits
    template <>
    struct FieldTraits<INT16>
    {
        typedef mama_i16_t MamaType;

        static void set(Field* field, MamaType value) 
        { 
            field->set_valint16(static_cast<int32_t>(value)); 
        }
        
        static MamaType get(const Field& field) 
        { 
            return static_cast<MamaType>(field.valint16()); 
        }
 
    };
 
    // UINT16 traits
    template <>
    struct FieldTraits<UINT16>
    {
        typedef mama_u16_t MamaType;
 
        static void set(Field* field, MamaType value) 
        { 
            field->set_valuint16(static_cast<uint32_t>(value)); 
        }
        
        static MamaType get(const Field& field) 
        { 
            return static_cast<MamaType>(field.valuint16()); 
        }
    };
 
    // INT32 traits
    template <>
    struct FieldTraits<INT32>
    {
        typedef mama_i32_t MamaType;
 
        static void set(Field* field, MamaType value) 
        { 
            field->set_valint32(static_cast<int32_t>(value)); 
        }
        
        static MamaType get(const Field& field) 
        { 
            return static_cast<MamaType>(field.valint32()); 
        }
    };
   
    // UINT132 traits
    template <>
    struct FieldTraits<UINT32>
    {
        typedef mama_u32_t MamaType;
 
        static void set(Field* field, MamaType value) 
        { 
            field->set_valuint32(static_cast<uint32_t>(value)); 
        }
        
        static MamaType get(const Field& field) 
        { 
            return static_cast<MamaType>(field.valuint32()); 
        }
    };
    
    // INT64 traits
    template <>
    struct FieldTraits<INT64>
    {
        typedef mama_i64_t MamaType;
 
        static void set(Field* field, MamaType value) 
        { 
            field->set_valint64(static_cast<int64_t>(value)); 
        }
        
        static MamaType get(const Field& field) 
        { 
            return static_cast<MamaType>(field.valint64()); 
        }
    };
 
    // UINT64 traits
    template <>
    struct FieldTraits<UINT64>
    {
        typedef mama_u64_t MamaType;
 
        static void set(Field* field, MamaType value) 
        { 
            field->set_valuint64(static_cast<uint64_t>(value)); 
        }
        
        static MamaType get(const Field& field) 
        { 
            return static_cast<MamaType>(field.valuint64()); 
        }
    };
    
    // FLOAT32 traits
    template <>
    struct FieldTraits<FLOAT32>
    {
        typedef mama_f32_t MamaType;
 
        static void set(Field* field, MamaType value) 
        { 
            field->set_valfloat32(static_cast<float>(value)); 
        }
        
        static MamaType get(const Field& field) 
        { 
            return static_cast<MamaType>(field.valfloat32()); 
        }
    };

    // FLOAT32 traits
    template <>
    struct FieldTraits<FLOAT64>
    {
        typedef mama_f64_t MamaType;
       
        static void set(Field* field, MamaType value) 
        { 
            field->set_valfloat64(static_cast<float>(value)); 
        }
        
        static MamaType get(const Field& field) 
        { 
            return static_cast<MamaType>(field.valfloat64()); 
        }
    };

    // STRING traits
    template <>
    struct FieldTraits<STRING>
    {
        typedef const char* MamaType;

        static void set(Field* field, MamaType value)
        {
            field->set_valstring(value);
        }

        static MamaType get(const Field& field)
        {
            return field.valstring().c_str();
        }
    };
 
    // OPAQUE traits
    template <>
    struct FieldTraits<OPAQUE>
    {
        typedef Opaque MamaType;

        static void set(Field* field, MamaType value)
        {
            field->set_valopaque(static_cast<const char*>(value.data), 
                static_cast<int>(value.size));
        }

        static MamaType get(const Field& field)
        {
            const std::string& buffer = field.valopaque();
            MamaType opaque(static_cast<const void*>(buffer.c_str()),
                static_cast<mama_size_t>(buffer.size()));
            return opaque;
        }
    };
}

#endif // PROTOBUF_PAYLOAD_IMPL_H_
