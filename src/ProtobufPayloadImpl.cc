#include <mama/msg.h>
#include <MamaPacket.pb.h>
#include <ProtobufPayloadImpl.h>

using namespace ProtobufPayload;

Impl::Impl()
    : m_parent(NULL)
    , m_packet(new Packet())
{
}

Impl::Impl(Packet* packet)
    : m_parent(NULL)
    , m_packet(packet)
{
}

Impl::Impl(const Impl& other)
    : m_parent(other.m_parent)
    , m_fidLookup(other.m_fidLookup)
    , m_nameLookup(other.m_nameLookup)
    , m_packet(new ProtobufPayload::Packet(*other.m_packet))
{   
}

Impl& Impl::operator = (const Impl& other)
{
    this->m_parent     = other.m_parent;
    this->m_fidLookup  = other.m_fidLookup;
    this->m_nameLookup = other.m_nameLookup;
    this->m_packet     = new ProtobufPayload::Packet(*other.m_packet);
}

Impl::~Impl()
{
    delete m_packet;
}

Packet* Impl::getRawPacket()
{
    return m_packet;
}

void Impl::clear()
{
    m_packet->Clear();
}

void Impl::setParent(const mamaMsg parent)
{ 
    m_parent = parent;
}

uint32_t Impl::getNumFields()
{
    return m_packet->fields_size();
}

const char* Impl::toString()
{
    return m_packet->SerializeAsString().c_str();
}

bool Impl::serialize(const void** buffer, 
                     mama_size_t* length)
{
    *(char*)(*buffer) = MAMA_PAYLOAD_PROTOBUF;
    bool result = m_packet->SerializeToArray(buffer + sizeof(char), static_cast<int>(*length));
    *length++;
    return result;
}

bool Impl::unSerialize(const void* buffer,
                       mama_size_t length)
{
    length--;
    const char* tmp = static_cast<const char*>(buffer);
    bool result = m_packet->ParseFromArray(static_cast<const void*>(++tmp), static_cast<int>(length));
    // TODO: Iterate fields and populate m_fidLookup and m_nameLookup
    return result;
}
