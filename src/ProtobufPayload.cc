#include <mama/mama.h>
#include <mama/fielddesc.h>
#include <mama/dictionary.h>
#include <payloadbridge.h>
#include <ProtobufPayloadConfig.h>
#include <ProtobufPayloadTypes.h>
#include <ProtobufPayloadImpl.h>
#include <ProtobufPayload.h>

// NULL checking on debug builds or if PROTOBUF_CHECK_NULLS is defined
#if (defined(PROTOBUF_CHECK_NULLS) || !defined(NDEBUG))
    #define NULL_CHECK(ptr) do { if (!ptr) return MAMA_STATUS_NULL_ARG; } while (0)
#else
    #define NULL_CHECK(ptr) 0
#endif

// Bring in Protobuf package
using namespace ProtobufPayload;

// Bridge functions
// ****************

mama_status 
protobufPayload_createImpl(mamaPayloadBridge* result,
                           char*              identifier)
{
    mamaPayloadBridgeImpl* impl   = NULL;
    mama_status            status = MAMA_STATUS_OK;

    if (!result) return MAMA_STATUS_NULL_ARG;

    // Log payload version
    mama_log(MAMA_LOG_LEVEL_NORMAL, "protobufPayload_createImpl(): "
        "Initializing MAMA Protocol Buffers Payload v" 
        STR(PROTOBUF_PAYLOAD_MINOR) "."
        STR(PROTOBUF_PAYLOAD_MAJOR));

    // Allocate bridge impl
    impl = (mamaPayloadBridgeImpl*)calloc(1, sizeof(mamaPayloadBridgeImpl));
    if (!impl)
    {
        mama_log(MAMA_LOG_LEVEL_SEVERE, "protobufPayload_createImpl(): "
            "Could not allocate memory for payload impl.");
        return MAMA_STATUS_NULL_ARG;
    }

    // Initialize bridge functions
    INITIALIZE_PAYLOAD_BRIDGE(impl, protobuf);

    impl->mClosure = NULL;

    *result     = static_cast<mamaPayloadBridge>(impl);
    *identifier = MAMA_PAYLOAD_PROTOBUF;

    return status;
}

mama_status 
protobufPayload_destroyImpl(mamaPayloadBridge impl)
{
    NULL_CHECK(impl);
    if (impl)
    {
        free(static_cast<mamaPayloadBridgeImpl*>(impl));
        return MAMA_STATUS_OK;
    }
    else return MAMA_STATUS_NULL_ARG;
}

mamaPayloadType 
protobufPayload_getType(void)
{
    return MAMA_PAYLOAD_PROTOBUF;
}

// General functions
// *****************

mama_status 
protobufPayload_create(msgPayload* msg)
{
    NULL_CHECK(msg);
    Impl* protobufImpl = new Impl();
    *msg = static_cast<msgPayload>(protobufImpl);
    return MAMA_STATUS_OK;
}

mama_status 
protobufPayload_createForTemplate (msgPayload*       msg,
                                   mamaPayloadBridge bridge,
                                   mama_u32_t        templateId)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status 
protobufPayload_createFromByteBuffer(msgPayload*       msg, 
                                     mamaPayloadBridge bridge, 
                                     const void*       buffer, 
                                     mama_size_t       bufferLength)
{
    Impl* protobufImpl = 
        new Impl(static_cast<Packet*>(const_cast<void*>(buffer)));
    *msg = static_cast<msgPayload>(protobufImpl);
    return MAMA_STATUS_OK;
}

mama_status 
protobufPayload_copy(const msgPayload msg,
                     msgPayload*      copy)
{
    NULL_CHECK(msg);
    if (!*copy) protobufPayload_create(copy);
    *copy = static_cast<msgPayload>(
        new Packet(*static_cast<Packet*>(msg)));
    return MAMA_STATUS_OK;
}

mama_status 
protobufPayload_clear(msgPayload msg)
{
    NULL_CHECK(msg);
    static_cast<Impl*>(msg)->clear();
    return MAMA_STATUS_OK;
}

mama_status 
protobufPayload_destroy(msgPayload msg)
{
    NULL_CHECK(msg);
    delete static_cast<Impl*>(msg);
    return MAMA_STATUS_OK;
}

mama_status 
protobufPayload_setParent (msgPayload    msg,                           
                           const mamaMsg parent)
{
    NULL_CHECK(msg);
    static_cast<Impl*>(msg)->setParent(parent);
    return MAMA_STATUS_OK;
}

mama_status
protobufPayload_getByteSize(const msgPayload msg,
                             mama_size_t*    size)
{
    *size = 0;
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getNumFields(const msgPayload msg,
                             mama_size_t*     numFields)
{
    NULL_CHECK(msg);
    *numFields = static_cast<Impl*>(msg)->getNumFields();
    return MAMA_STATUS_OK;
}

mama_status
protobufPayload_getSendSubject(const msgPayload msg,
                               const char**     subject)
{
    NULL_CHECK(msg);
    return protobufPayload_getString(msg, PROTOBUF_SUBJ_FIELD, 0, subject);
}

const char*
protobufPayload_toString(const msgPayload msg)
{
    return static_cast<Impl*>(msg)->toString();
}

mama_status
protobufPayload_iterateFields(const msgPayload  msg,
                              const mamaMsg     parent,
                              mamaMsgField      field,
                              mamaMsgIteratorCb cb,
                              void*             closure)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getFieldAsString(const msgPayload msg,
                                 const char*      name,
                                 mama_fid_t       fid,
                                 char*            buf,
                                 mama_size_t      len)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status 
protobufPayload_serialize(const msgPayload msg,
                          const void**     buffer,
                          mama_size_t*     length)
{
    NULL_CHECK(msg);
    return (static_cast<Impl*>(msg)->serialize(buffer, length))
        ? MAMA_STATUS_OK : MAMA_STATUS_PLATFORM;
}

mama_status 
protobufPayload_unSerialize(const msgPayload msg,
                            const void*      buffer,
                            mama_size_t      length)
{
    NULL_CHECK(msg);
    return (static_cast<Impl*>(msg)->unSerialize(buffer, length))
        ? MAMA_STATUS_OK : MAMA_STATUS_PLATFORM;
}

mama_status
protobufPayload_getByteBuffer(const msgPayload msg,
                              const void**     buffer,
                              mama_size_t*     length)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_setByteBuffer(const msgPayload  msg,
                              mamaPayloadBridge bridge,
                              const void*       buffer,
                              mama_size_t       bufferLength)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}


mama_status
protobufPayload_apply(msgPayload       dest,
                      const msgPayload src)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getNativeMsg(const msgPayload msg,
                             void**           nativeMsg)
{
    NULL_CHECK(msg);
    *nativeMsg = static_cast<void*>(static_cast<Impl*>(msg)->getRawPacket());
    return MAMA_STATUS_OK;
}

// Add functions
// *************

mama_status
protobufPayload_addBool(msgPayload  msg,
                        const char* name,
                        mama_fid_t  fid,
                        mama_bool_t value)
{
    return protobufPayload_updateBool(msg, name, fid, value);
}

mama_status
protobufPayload_addChar(msgPayload  msg,
                        const char* name,
                        mama_fid_t  fid,
                        char        value)
{
    return protobufPayload_updateChar(msg, name, fid, value);
}

mama_status
protobufPayload_addI8(msgPayload  msg,
                      const char* name,
                      mama_fid_t  fid,
                      mama_i8_t   value)
{ 
    return protobufPayload_updateI8(msg, name, fid, value);
}

mama_status
protobufPayload_addU8(msgPayload  msg,
                      const char* name,
                      mama_fid_t  fid,
                      mama_u8_t   value)
{ 
    return protobufPayload_updateU8(msg, name, fid, value);
}

mama_status
protobufPayload_addI16(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_i16_t  value)
{
    return protobufPayload_updateI16(msg, name, fid, value);
}

mama_status
protobufPayload_addU16(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_u16_t  value)
{
    return protobufPayload_updateU16(msg, name, fid, value);
}

mama_status
protobufPayload_addI32(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_i32_t  value)
{
    return protobufPayload_updateI32(msg, name, fid, value);
}

mama_status
protobufPayload_addU32(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_u32_t  value)
{
    return protobufPayload_updateU32(msg, name, fid, value);
}

mama_status
protobufPayload_addI64(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_i64_t  value)
{
    return protobufPayload_updateI64(msg, name, fid, value);
}

mama_status
protobufPayload_addU64(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_u64_t  value)
{
    return protobufPayload_updateU64(msg, name, fid, value);
}

mama_status
protobufPayload_addF32(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_f32_t  value)
{
    return protobufPayload_updateF32(msg, name, fid, value);
}

mama_status
protobufPayload_addF64(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       mama_f64_t  value)
{
    return protobufPayload_updateF64(msg, name, fid, value);
}

mama_status
protobufPayload_addString(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          const char* value)
{
    return protobufPayload_updateString(msg, name, fid, value);
}

mama_status
protobufPayload_addOpaque(msgPayload          msg,
                          const char*         name,
                          mama_fid_t          fid,
                          const void*         value,
                          mama_size_t         size)
{
    return protobufPayload_updateOpaque(msg, name, fid, value, size);
}

mama_status
protobufPayload_addDateTime(msgPayload         msg,
                            const char*        name,
                            mama_fid_t         fid,
                            const mamaDateTime value)
{
    return protobufPayload_updateDateTime(msg, name, fid, value);
}

mama_status
protobufPayload_addPrice(msgPayload      msg,
                         const char*     name,
                         mama_fid_t      fid,
                         const mamaPrice value)
{
    return protobufPayload_updatePrice(msg, name, fid, value);
}

mama_status
protobufPayload_addMsg(msgPayload  msg,
                       const char* name,
                       mama_fid_t  fid,
                       msgPayload  value)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

// Add vector functions
// ********************

mama_status
protobufPayload_addVectorBool(msgPayload        msg,
                              const char*       name,
                              mama_fid_t        fid,
                              const mama_bool_t value[],
                              mama_size_t       size)
{
    return protobufPayload_updateVectorBool(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorChar(msgPayload  msg,
                              const char* name,
                              mama_fid_t  fid,
                              const char  value[],
                              mama_size_t size)
{
    return protobufPayload_updateVectorChar(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorI8(msgPayload      msg,
                            const char*     name,
                            mama_fid_t      fid,
                            const mama_i8_t value[],
                            mama_size_t     size)
{
    return protobufPayload_updateVectorI8(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorU8(msgPayload      msg,
                            const char*     name,
                            mama_fid_t      fid,
                            const mama_u8_t value[],
                            mama_size_t     size)
{
    return protobufPayload_updateVectorU8(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorI16(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_i16_t value[],
                             mama_size_t      size)
{
    return protobufPayload_updateVectorI16(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorU16(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_u16_t value[],
                             mama_size_t      size)
{
    return protobufPayload_updateVectorU16(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorI32(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_i32_t value[],
                             mama_size_t      size)
{
    return protobufPayload_updateVectorI32(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorU32(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_u32_t value[],
                             mama_size_t      size)
{
    return protobufPayload_updateVectorU32(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorI64(msgPayload    msg,
                             const char*   name,
                             mama_fid_t    fid,
                             const int64_t value[],
                             mama_size_t   size)
{
    return protobufPayload_updateVectorI64(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorU64(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_u64_t value[],
                             mama_size_t      size)
{
    return protobufPayload_updateVectorU64(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorF32(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_f32_t value[],
                             mama_size_t      size)
{
    return protobufPayload_updateVectorF32(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorF64(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const mama_f64_t value[],
                             mama_size_t      size)
{
    return protobufPayload_updateVectorF64(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorString(msgPayload  msg,
                                const char* name,
                                mama_fid_t  fid,
                                const char* value[],
                                mama_size_t size)
{
    return protobufPayload_updateVectorString(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorMsg(msgPayload    msg,
                             const char*   name,
                             mama_fid_t    fid,
                             const mamaMsg value[],
                             mama_size_t   size)
{
    return protobufPayload_updateVectorMsg(msg, name, fid, value, size);
}

mama_status
protobufPayload_addVectorDateTime(msgPayload         msg,
                                  const char*        name,
                                  mama_fid_t         fid,
                                  const mamaDateTime value[],
                                  mama_size_t        size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_addVectorPrice(msgPayload      msg,
                               const char*     name,
                               mama_fid_t      fid,
                               const mamaPrice value[],
                               mama_size_t     size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
//    return protobufPayload_updateVectorPrice(msg, name, fid, value, size);
}

// Update functions
// ****************

mama_status
protobufPayload_updateBool(msgPayload  msg,
                           const char* name,
                           mama_fid_t  fid,
                           mama_bool_t value)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        setField<BOOL>(name, fid, value);
}

mama_status
protobufPayload_updateChar(msgPayload  msg,
                           const char* name,
                           mama_fid_t  fid,
                           char        value)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        setField<CHAR>(name, fid, value);
}

mama_status
protobufPayload_updateI8(msgPayload  msg,
                         const char* name,
                         mama_fid_t  fid,
                         mama_i8_t   value)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        setField<INT8>(name, fid, value);
}

mama_status
protobufPayload_updateU8(msgPayload  msg,
                         const char* name,
                         mama_fid_t  fid,
                         mama_u8_t   value)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        setField<UINT8>(name, fid, value);
}

mama_status
protobufPayload_updateI16(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_i16_t  value)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        setField<INT16>(name, fid, value);
}

mama_status
protobufPayload_updateU16(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_u16_t  value)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        setField<UINT16>(name, fid, value);
}

mama_status
protobufPayload_updateI32(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_i32_t  value)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        setField<INT32>(name, fid, value);
}

mama_status
protobufPayload_updateU32(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_u32_t  value)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        setField<UINT32>(name, fid, value);
}

mama_status
protobufPayload_updateI64(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_i64_t  value)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        setField<INT64>(name, fid, value);
}

mama_status
protobufPayload_updateU64(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_u64_t  value)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        setField<UINT64>(name, fid, value);
}

mama_status
protobufPayload_updateF32(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_f32_t  value)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        setField<FLOAT32>(name, fid, value);
}

mama_status
protobufPayload_updateF64(msgPayload  msg,
                          const char* name,
                          mama_fid_t  fid,
                          mama_f64_t  value)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        setField<FLOAT64>(name, fid, value);
}

mama_status
protobufPayload_updateString(msgPayload  msg,
                             const char* name,
                             mama_fid_t  fid,
                             const char* value)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        setField<STRING>(name, fid, value);
}

mama_status
protobufPayload_updateOpaque(msgPayload  msg,
                             const char* name,
                             mama_fid_t  fid,
                             const void* data,
                             mama_size_t size)
{
    NULL_CHECK(msg);
    Opaque opaque(data, size);
    return static_cast<Impl*>(msg)->
        setField<OPAQUE>(name, fid, opaque);
}

mama_status
protobufPayload_updateDateTime(msgPayload         msg,
                               const char*        name,
                               mama_fid_t         fid,
                               const mamaDateTime value)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updatePrice(msgPayload      msg,
                            const char*     name,
                            mama_fid_t      fid,
                            const mamaPrice value)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateSubMsg(msgPayload       msg,
                             const char*      name,
                             mama_fid_t       fid,
                             const msgPayload value)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

// Update vector functions
// ***********************

mama_status
protobufPayload_updateVectorMsg(msgPayload          msg,
                                const char*         name,
                                mama_fid_t          fid,
                                const mamaMsg       value[],
                                mama_size_t         size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorString(msgPayload  msg,
                                   const char* name,
                                   mama_fid_t  fid,
                                   const char* strList[],
                                   mama_size_t size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorBool(msgPayload        msg,
                                 const char*       name,
                                 mama_fid_t        fid,
                                 const mama_bool_t boolList[],
                                 mama_size_t       size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorChar(msgPayload  msg,
                                 const char* name,
                                 mama_fid_t  fid,
                                 const char  value[],
                                 mama_size_t size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorI8(msgPayload      msg,
                               const char*     name,
                               mama_fid_t      fid,
                               const mama_i8_t value[],
                               mama_size_t     size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorU8(msgPayload      msg,
                               const char*     name,
                               mama_fid_t      fid,
                               const mama_u8_t value[],
                               mama_size_t     size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorI16(msgPayload       msg,
                                const char*      name,
                                mama_fid_t       fid,
                                const mama_i16_t value[],
                                mama_size_t      size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorU16(msgPayload       msg,
                                const char*      name,
                                mama_fid_t       fid,
                                const mama_u16_t value[],
                                mama_size_t      size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorI32(msgPayload       msg,
                                const char*      name,
                                mama_fid_t       fid,
                                const mama_i32_t value[],
                                mama_size_t      size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorU32(msgPayload       msg,
                                const char*      name,
                                mama_fid_t       fid,
                                const mama_u32_t value[],
                                mama_size_t      size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorI64(msgPayload       msg,
                                const char*      name,
                                mama_fid_t       fid,
                                const mama_i64_t value[],
                                mama_size_t      size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorU64(msgPayload       msg,
                                const char*      name,
                                mama_fid_t       fid,
                                const mama_u64_t value[],
                                mama_size_t      size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorF32(msgPayload       msg,
                                const char*      name,
                                mama_fid_t       fid,
                                const mama_f32_t value[],
                                mama_size_t      size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorF64(msgPayload       msg,
                                const char*      name,
                                mama_fid_t       fid,
                                const mama_f64_t value[],
                                mama_size_t      size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorPrice(msgPayload       msg,
                                  const char*      name,
                                  mama_fid_t       fid,
                                  const mamaPrice* value[],
                                  mama_size_t      size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_updateVectorTime(msgPayload         msg,
                                 const char*        name,
                                 mama_fid_t         fid,
                                 const mamaDateTime value[],
                                 mama_size_t        size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

// Get functions
// *************

mama_status
protobufPayload_getBool(const msgPayload msg,
                        const char*      name,
                        mama_fid_t       fid,
                        mama_bool_t*     result)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        getField<BOOL>(name, fid, *result);
}

mama_status
protobufPayload_getChar(const msgPayload msg,
                        const char*      name,
                        mama_fid_t       fid,
                        char*            result)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        getField<CHAR>(name, fid, *result);
}

mama_status
protobufPayload_getI8(const msgPayload msg,
                      const char*      name,
                      mama_fid_t       fid,
                      mama_i8_t*       result)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        getField<INT8>(name, fid, *result);
}

mama_status
protobufPayload_getU8(const msgPayload msg,
                      const char*      name,
                      mama_fid_t       fid,
                      mama_u8_t*       result)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        getField<UINT8>(name, fid, *result);
}

mama_status
protobufPayload_getI16(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_i16_t*      result)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        getField<INT16>(name, fid, *result);
}

mama_status
protobufPayload_getU16(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_u16_t*      result)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        getField<UINT16>(name, fid, *result);
}

mama_status
protobufPayload_getI32(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_i32_t*      result)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        getField<INT32>(name, fid, *result);
}

mama_status
protobufPayload_getU32(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_u32_t*      result)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        getField<UINT32>(name, fid, *result);
    return MAMA_STATUS_OK;
}

mama_status
protobufPayload_getI64(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_i64_t*      result)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        getField<INT64>(name, fid, *result);
    return MAMA_STATUS_OK;
}

mama_status
protobufPayload_getU64(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_u64_t*      result)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        getField<UINT64>(name, fid, *result);
    return MAMA_STATUS_OK;
}

mama_status
protobufPayload_getF32(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_f32_t*      result)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        getField<FLOAT32>(name, fid, *result);
}

mama_status
protobufPayload_getF64(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       mama_f64_t*      result)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        getField<FLOAT64>(name, fid, *result);
    return MAMA_STATUS_OK;
}

mama_status
protobufPayload_getString(const msgPayload msg,
                          const char*      name,
                          mama_fid_t       fid,
                          const char**     result)
{
    NULL_CHECK(msg);
    return static_cast<Impl*>(msg)->
        getField<STRING>(name, fid, *result);
    return MAMA_STATUS_OK;
}

mama_status
protobufPayload_getOpaque(const msgPayload msg,
                          const char*      name,
                          mama_fid_t       fid,
                          const void**     data,
                          mama_size_t*     size)
{
    NULL_CHECK(msg);
    Opaque opaque;
    mama_status status = static_cast<Impl*>(msg)->
        getField<OPAQUE>(name, fid, opaque);
    *data = opaque.data;
    *size = opaque.size;
    return status;
}

mama_status
protobufPayload_getField(const msgPayload msg,
                         const char*      name,
                         mama_fid_t       fid,
                         msgFieldPayload* result)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getDateTime(const msgPayload msg,
                            const char*      name,
                            mama_fid_t       fid,
                            mamaDateTime     result)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getPrice(const msgPayload msg,
                         const char*      name,
                         mama_fid_t       fid,
                         mamaPrice        result)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getMsg(const msgPayload msg,
                       const char*      name,
                       mama_fid_t       fid,
                       msgPayload*      result)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

// Get vector dunctions
// ********************

mama_status
protobufPayload_getVectorBool(const msgPayload    msg,
                              const char*         name,
                              mama_fid_t          fid,
                              const mama_bool_t** result,
                              mama_size_t*        size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorChar(const msgPayload msg,
                              const char*      name,
                              mama_fid_t       fid,
                              const char**     result,
                              mama_size_t*     size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorI8(const msgPayload  msg,
                            const char*       name,
                            mama_fid_t        fid,
                            const mama_i8_t** result,
                            mama_size_t*      size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorU8(const msgPayload  msg,
                            const char*       name,
                            mama_fid_t        fid,
                            const mama_u8_t** result,
                            mama_size_t*      size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorI16(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_i16_t** result,
                             mama_size_t*       size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorU16(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_u16_t** result,
                             mama_size_t*       size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorI32(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_i32_t** result,
                             mama_size_t*       size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorU32(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_u32_t** result,
                             mama_size_t*       size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorI64(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_i64_t** result,
                             mama_size_t*       size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorU64(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_u64_t** result,
                             mama_size_t*       size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorF32(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_f32_t** result,
                             mama_size_t*       size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorF64(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const mama_f64_t** result,
                             mama_size_t*       size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorString(const msgPayload msg,
                                const char*      name,
                                mama_fid_t       fid,
                                const char***    result,
                                mama_size_t*     size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorDateTime(const msgPayload    msg,
                                  const char*         name,
                                  mama_fid_t          fid,
                                  const mamaDateTime* result,
                                  mama_size_t*        size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayload_getVectorPrice(const msgPayload msg,
                               const char*      name,
                               mama_fid_t       fid,
                               const mamaPrice* result,
                               mama_size_t*     size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}


mama_status
protobufPayload_getVectorMsg(const msgPayload   msg,
                             const char*        name,
                             mama_fid_t         fid,
                             const msgPayload** result,
                             mama_size_t*       size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED; 
}

// Iterator functions
// ******************

mama_status
protobufPayloadIter_create(msgPayloadIter* itr,
                           msgPayload      msg)
{
    NULL_CHECK(msg);
    *itr = static_cast<msgPayloadIter>(
        new FieldVector::iterator(static_cast<Packet*>(msg)->mutable_fields()->begin()));
    return MAMA_STATUS_OK;
}

msgFieldPayload
protobufPayloadIter_get(msgPayloadIter  itr,
                        msgFieldPayload field,
                        msgPayload      msg)
{
    if (!itr || !msg || !field) return NULL;

    // TODO: Skip __subj

    field = static_cast<msgFieldPayload>(
        &**static_cast<FieldVector::iterator*>(itr));

    return field;
}

msgFieldPayload
protobufPayloadIter_next(msgPayloadIter  itr,
                         msgFieldPayload field,
                         msgPayload      msg)
{
    if (!itr || !msg || !field) return NULL;

    if (++(*static_cast<FieldVector::iterator*>(itr)) == 
        static_cast<Packet*>(msg)->mutable_fields()->end())

        // We've reached the end of the vector
        return NULL;

    return static_cast<msgFieldPayload>(
        &**static_cast<FieldVector::iterator*>(itr));
}

mama_bool_t
protobufPayloadIter_hasNext(msgPayloadIter itr,
                            msgPayload     msg)
{
    return ((*static_cast<FieldVector::iterator*>(itr))+1 !=
        static_cast<Packet*>(msg)->mutable_fields()->end());
}

msgFieldPayload
protobufPayloadIter_begin(msgPayloadIter  itr,
                          msgFieldPayload field,
                          msgPayload      msg)
{
    if (!itr) return NULL;
    return static_cast<msgFieldPayload>(
        &*static_cast<Packet*>(msg)->mutable_fields()->begin());
}

msgFieldPayload
protobufPayloadIter_end(msgPayloadIter itr,
                        msgPayload     msg)
{
    return NULL;
}

mama_status
protobufPayloadIter_associate(msgPayloadIter itr,
                              msgPayload     msg)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufPayloadIter_destroy(msgPayloadIter itr)
{
    NULL_CHECK(itr);
    delete static_cast<FieldVector::iterator*>(itr);
    return MAMA_STATUS_OK;
}

// General field functions
// ***********************
mama_status
protobufFieldPayload_create(msgFieldPayload* field)
{
    *field = static_cast<msgFieldPayload>(new Field());
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_destroy(msgFieldPayload field)
{
    NULL_CHECK(field);
    delete static_cast<Field*>(field);
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getName(const msgFieldPayload field,
                             mamaDictionary        dict,
                             mamaFieldDescriptor   desc,
                             const char**          result)
{
    NULL_CHECK(field);

    mama_fid_t fid = static_cast<mama_fid_t>(static_cast<Field*>(field)->fid());

    if (fid != 0)
    {
        if (!desc)
        {
            if (dict)
            {
                mama_status status = 
                    mamaDictionary_getFieldDescriptorByFid(dict, &desc, fid);
                if (status != MAMA_STATUS_OK) return status;
                *result = mamaFieldDescriptor_getName(desc);
            }
            else *result = static_cast<Field*>(field)->name().c_str();
        }
        else *result = mamaFieldDescriptor_getName(desc);
    }
    else *result = static_cast<Field*>(field)->name().c_str();

    return (*result) ? MAMA_STATUS_OK : MAMA_STATUS_INVALID_ARG;
}

mama_status
protobufFieldPayload_getFid(const msgFieldPayload field,
                            mamaDictionary        dict,
                            mamaFieldDescriptor   desc,
                            uint16_t*             result)
{
    NULL_CHECK(field);

    mama_fid_t fid = static_cast<mama_fid_t>(static_cast<Field*>(field)->fid());

    if (fid == 0)
    {
    	if (!desc)
		{
    		if (dict)
			{
                mama_status status =
                    mamaDictionary_getFieldDescriptorByName(dict, &desc,
                        static_cast<Field*>(field)->name().c_str());
                if (status != MAMA_STATUS_OK) return status;
                *result = mamaFieldDescriptor_getFid(desc);
            }
            else *result = 0;
        }
        else *result = mamaFieldDescriptor_getFid(desc);
    }
    else *result = fid;
    
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getDescriptor(const msgFieldPayload field,
                                   mamaDictionary        dict,
                                   mamaFieldDescriptor*  result)
{
    NULL_CHECK(field);

    if (static_cast<Field*>(field)->name().empty())
        return MAMA_STATUS_INVALID_ARG;

    mama_fid_t fid = static_cast<mama_fid_t>(static_cast<Field*>(field)->fid());

    return (fid) ? mamaDictionary_getFieldDescriptorByFid(dict, result, fid)
        : mamaDictionary_getFieldDescriptorByName(dict, result, 
            static_cast<Field*>(field)->name().c_str());
}

mama_status
protobufFieldPayload_getType(msgFieldPayload field,
                             mamaFieldType*  result)
{
    NULL_CHECK(field);

    switch (static_cast<Field*>(field)->type())
    {
        case BOOL:    *result = MAMA_FIELD_TYPE_BOOL;    break;
        case CHAR:    *result = MAMA_FIELD_TYPE_CHAR;    break;
        case INT8:    *result = MAMA_FIELD_TYPE_I8;      break;
        case UINT8:   *result = MAMA_FIELD_TYPE_U8;      break;
        case INT16:   *result = MAMA_FIELD_TYPE_I16;     break;
        case UINT16:  *result = MAMA_FIELD_TYPE_U16;     break;
        case INT32:   *result = MAMA_FIELD_TYPE_I32;     break;
        case UINT32:  *result = MAMA_FIELD_TYPE_U32;     break;
        case INT64:   *result = MAMA_FIELD_TYPE_I64;     break;
        case UINT64:  *result = MAMA_FIELD_TYPE_U64;     break;
        case FLOAT32: *result = MAMA_FIELD_TYPE_F32;     break;
        case FLOAT64: *result = MAMA_FIELD_TYPE_F64;     break;
        case STRING:  *result = MAMA_FIELD_TYPE_STRING;  break;
        case OPAQUE:  *result = MAMA_FIELD_TYPE_OPAQUE;  break;
        default:      *result = MAMA_FIELD_TYPE_UNKNOWN; break;
    }

    return MAMA_STATUS_OK;
}

// Field update functions
// **********************
mama_status
protobufFieldPayload_updateBool(msgFieldPayload field,
                                msgPayload      msg,
                                mama_bool_t     value)
{
    NULL_CHECK(field);
    return protobufPayload_updateBool(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

mama_status
protobufFieldPayload_updateChar(msgFieldPayload field,
                                msgPayload      msg,
                                char            value)
{
    NULL_CHECK(field);
    return protobufPayload_updateChar(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

mama_status
protobufFieldPayload_updateU8(msgFieldPayload field,
                              msgPayload      msg,
                              mama_u8_t       value)
{
    NULL_CHECK(field);
    return protobufPayload_updateU8(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

mama_status
protobufFieldPayload_updateI8(msgFieldPayload field,
                              msgPayload      msg,
                              mama_i8_t       value)
{
    NULL_CHECK(field);
    return protobufPayload_updateI8(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

mama_status
protobufFieldPayload_updateI16(msgFieldPayload field,
                               msgPayload      msg,
                               mama_i16_t      value)
{
    NULL_CHECK(field);
    return protobufPayload_updateI16(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

mama_status
protobufFieldPayload_updateU16(msgFieldPayload field,
                               msgPayload      msg,
                               mama_u16_t      value)
{
    NULL_CHECK(field);
    return protobufPayload_updateU16(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

mama_status
protobufFieldPayload_updateI32(msgFieldPayload field,
                               msgPayload      msg,
                               mama_i32_t      value)
{
    NULL_CHECK(field);
    return protobufPayload_updateI32(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

mama_status
protobufFieldPayload_updateU32(msgFieldPayload field,
                               msgPayload      msg,
                               mama_u32_t      value)
{
    NULL_CHECK(field);
    return protobufPayload_updateU32(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

mama_status
protobufFieldPayload_updateI64(msgFieldPayload field,
                               msgPayload      msg,
                               mama_i64_t      value)
{
    NULL_CHECK(field);
    return protobufPayload_updateI64(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

mama_status
protobufFieldPayload_updateU64(msgFieldPayload field,
                               msgPayload      msg,
                               mama_u64_t      value)
{
    NULL_CHECK(field);
    return protobufPayload_updateU64(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

mama_status
protobufFieldPayload_updateF32(msgFieldPayload field,
                               msgPayload      msg,
                               mama_f32_t      value)
{
    NULL_CHECK(field);
    return protobufPayload_updateF32(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

mama_status
protobufFieldPayload_updateF64(msgFieldPayload field,
                               msgPayload      msg,
                               mama_f64_t      value)
{
    NULL_CHECK(field);
    return protobufPayload_updateF64(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

mama_status
protobufFieldPayload_updateDateTime(msgFieldPayload    field,
                                    msgPayload         msg,
                                    const mamaDateTime value)
{
    NULL_CHECK(field);
    return protobufPayload_updateDateTime(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

mama_status
protobufFieldPayload_updatePrice(msgFieldPayload field,
                                 msgPayload      msg,
                                 const mamaPrice value)
{
    NULL_CHECK(field);
    return protobufPayload_updatePrice(msg, static_cast<Field*>(field)->name().c_str(), 
        static_cast<mama_fid_t>(static_cast<Field*>(field)->fid()), value);
}

// Field get functions
// *******************
mama_status
protobufFieldPayload_getBool(const msgFieldPayload field,
                             mama_bool_t*          result)
{
    NULL_CHECK(field);
    *result = FieldTraits<BOOL>::get(*static_cast<Field*>(field));
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getChar(const msgFieldPayload field,
                             char*                 result)
{
    NULL_CHECK(field);
    *result = FieldTraits<CHAR>::get(*static_cast<Field*>(field));
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getI8(const msgFieldPayload field,
                           mama_i8_t*            result)
{
    NULL_CHECK(field);
    *result = FieldTraits<INT8>::get(*static_cast<Field*>(field));
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getU8(const msgFieldPayload field,
                           mama_u8_t*            result)
{
    NULL_CHECK(field);
    *result = FieldTraits<UINT8>::get(*static_cast<Field*>(field));
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getI16(const msgFieldPayload field,
                            mama_i16_t*           result)
{
    NULL_CHECK(field);
    *result = FieldTraits<INT16>::get(*static_cast<Field*>(field));
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getU16(const msgFieldPayload field,
                            mama_u16_t*           result)
{
    NULL_CHECK(field);
    *result = FieldTraits<UINT16>::get(*static_cast<Field*>(field));
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getI32(const msgFieldPayload field,
                            mama_i32_t*           result)
{
    NULL_CHECK(field);
    *result = FieldTraits<INT32>::get(*static_cast<Field*>(field));
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getU32(const msgFieldPayload field,
                            mama_u32_t*           result)
{
    NULL_CHECK(field);
    *result = FieldTraits<UINT32>::get(*static_cast<Field*>(field));
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getI64(const msgFieldPayload field,
                            mama_i64_t*           result)
{
    NULL_CHECK(field);
    *result = FieldTraits<INT64>::get(*static_cast<Field*>(field));
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getU64(const msgFieldPayload field,
                            mama_u64_t*           result)
{
    NULL_CHECK(field);
    *result = FieldTraits<UINT64>::get(*static_cast<Field*>(field));
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getF32(const msgFieldPayload field,
                            mama_f32_t*           result)
{
    NULL_CHECK(field);
    *result = FieldTraits<FLOAT64>::get(*static_cast<Field*>(field));
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getF64(const msgFieldPayload field,
                            mama_f64_t*           result)
{
    NULL_CHECK(field);
    *result = FieldTraits<FLOAT64>::get(*static_cast<Field*>(field));
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getString(const msgFieldPayload field,
                               const char**          result)
{
    NULL_CHECK(field);
    *result = FieldTraits<STRING>::get(*static_cast<Field*>(field));
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getOpaque(const msgFieldPayload field,
                               const void**          result,
                               mama_size_t*          size)
{
    NULL_CHECK(field);
    Opaque opaque = FieldTraits<OPAQUE>::get(*static_cast<Field*>(field));
    *result = opaque.data;
    *size   = opaque.size;
    return MAMA_STATUS_OK;
}

mama_status
protobufFieldPayload_getDateTime(const msgFieldPayload field,
                                 mamaDateTime          result)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getPrice(const msgFieldPayload field,
                              mamaPrice             result)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getMsg(const msgFieldPayload field,
                            msgPayload*           result)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorBool(const msgFieldPayload field,
                                   const mama_bool_t**   result,
                                   mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorChar(const msgFieldPayload field,
                                   const char**          result,
                                   mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorI8(const msgFieldPayload field,
                                 const mama_i8_t**     result,
                                 mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorU8(const msgFieldPayload field,
                                 const mama_u8_t**     result,
                                 mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorI16(const msgFieldPayload field,
                                  const mama_i16_t**    result,
                                  mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorU16 (const msgFieldPayload field,
                                   const mama_u16_t**    result,
                                   mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorI32 (const msgFieldPayload field,
                                   const mama_i32_t**    result,
                                   mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorU32 (const msgFieldPayload field,
                                   const mama_u32_t**    result,
                                   mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorI64 (const msgFieldPayload field,
                                   const mama_i64_t**    result,
                                   mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorU64 (const msgFieldPayload field,
                                   const mama_u64_t**    result,
                                   mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorF32 (const msgFieldPayload field,
                                   const mama_f32_t**    result,
                                   mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorF64 (const msgFieldPayload field,
                                   const mama_f64_t**    result,
                                   mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorString(const msgFieldPayload field,
                                     const char***         result,
                                     mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorDateTime(const msgFieldPayload field,
                                       const mamaDateTime*   result,
                                       mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorPrice(const msgFieldPayload field,
                                    const mamaPrice*      result,
                                    mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getVectorMsg(const msgFieldPayload field,
                                  const msgPayload**    result,
                                  mama_size_t*          size)
{
    return MAMA_STATUS_NOT_IMPLEMENTED;
}

mama_status
protobufFieldPayload_getAsString(const msgFieldPayload field,
                                 const msgPayload      msg,
                                 char*                 buf,
                                 mama_size_t           len)
{
    NULL_CHECK(field);
    std::string str = static_cast<Field*>(field)->SerializeAsString();
    buf = const_cast<char*>(str.c_str()); // Do I need to copy?
    len = str.size();
    return MAMA_STATUS_OK;
}
