#ifndef PROTOBUF_PAYLOAD_IMPL_H__
#define PROTOBUF_PAYLOAD_IMPL_H__

#include <map>
#include <string>
#include <mama/types.h>
#include <MamaPacket.pb.h>
#include <ProtobufPayloadTypes.h>
#include <ProtobufPayloadFieldTraits.h>

namespace ProtobufPayload
{
    class Impl
    {
    public:


        /**
         * Default constructor.
         */
        Impl();

        /**
         * Constructor with custom packet.
         * Allows construction with pre-allocated packet object, ownership of packet
         * passes to impl, so deletion of packet is handled internally.
         */
        explicit Impl(ProtobufPayload::Packet* packet);

        /**
         * Copy constructor.
         */
        explicit Impl(const Impl& other);

        /**
         * Assignment operator.
         */
        Impl& operator = (const Impl& other);

        /**
         * Destructor.
         * Deletes packet object.
         */
        ~Impl();
        
        /**
         * Raw packet getter.
         * Returns a pointer to the raw Protocol Buffers object that holds the data.
         */
        ProtobufPayload::Packet* 
        getRawPacket();

        /**
         * Clear fields.
         */
        void
        clear();

        /**
         * Set parent message.
         */
        void
        setParent(const mamaMsg parent);

        /**
         * Get number of fields.
         */
        uint32_t
        getNumFields();

        /**
         * Serialize message to a string.
         */
        const char*
        toString();
        
        /**
         * Serialize message to buffer.
         */
        bool 
        serialize(const void** buffer, 
                  mama_size_t* length);

        /**
         * De-serialize message from buffer.
         */
        bool
        unSerialize(const void* buffer, 
                    mama_size_t length);

        /**
         * Generic set accessor.
         * Wraps Protobuf access in a type-agnostic way.
         */
        template <ProtobufPayload::FieldType FIELD_TYPE> 
        mama_status 
        setField(const char*                                  name, 
                 mama_fid_t                                   fid, 
                 typename FieldTraits<FIELD_TYPE>::MamaType& value);

        /**
         * Generic get accessor.
         * Wraps Protobuf access in a type-agnostic way.
         */
        template <ProtobufPayload::FieldType FIELD_TYPE>
        mama_status
        getField(const char*                                 name, 
                 mama_fid_t                                  fid,
                 typename FieldTraits<FIELD_TYPE>::MamaType& result);

    private:

        mamaMsg m_parent;

        Packet* m_packet;

        std::map<mama_fid_t, int>  m_fidLookup;
        std::map<const char*, int> m_nameLookup;
    };

    template <FieldType FIELD_TYPE> 
    mama_status 
    Impl::setField(const char*                                 name,
                   mama_fid_t                                  fid,
                   typename FieldTraits<FIELD_TYPE>::MamaType& value)
    {
        // -1 implies not found
        int idx = -1;

        // Search for FID
        std::map<mama_fid_t, int>::iterator fidItr = m_fidLookup.find(fid);
        if (fidItr != m_fidLookup.end()) idx = fidItr->second;
            
        // Search for name if one was specified
        else if (name)
        {
            std::map<const char*, int>::iterator nameItr = m_nameLookup.find(name);
            if (nameItr != m_nameLookup.end()) idx = nameItr->second;
        }

        // Add new field
        if (idx == -1)
        {
            idx = m_packet->fields_size();
            
            m_fidLookup.insert(std::make_pair(fid, idx));
            
            if (name) m_nameLookup.insert(std::make_pair(name, idx));
            
            Field* field = m_packet->add_fields();
            
            field->set_type(FIELD_TYPE);
            field->set_fid(fid);
            field->set_name(name);

            FieldTraits<FIELD_TYPE>::set(field, value);
        }

        // Modify existing field
        else
        {
            Field* field = m_packet->mutable_fields(idx);
            FieldTraits<FIELD_TYPE>::set(field, value);
        }

        return MAMA_STATUS_OK;
    }

    template <FieldType FIELD_TYPE>
    mama_status
    Impl::getField(const char*                                 name,
                   mama_fid_t                                  fid,
                   typename FieldTraits<FIELD_TYPE>::MamaType& result)
    {
        // -1 implies not found
        int idx = -1;

        // Search for FID
        std::map<mama_fid_t, int>::iterator fidItr = m_fidLookup.find(fid);
        if (fidItr != m_fidLookup.end()) idx = fidItr->second;
            
        // Search for name if one was specified
        else if (name)
        {
            std::map<const char*, int>::iterator nameItr = m_nameLookup.find(name);
            if (nameItr != m_nameLookup.end()) idx = nameItr->second;
            else return MAMA_STATUS_NOT_FOUND;
        }

        else return MAMA_STATUS_NOT_FOUND;

        result = FieldTraits<FIELD_TYPE>::get(m_packet->fields(idx));

        return MAMA_STATUS_OK;
    }
}

#endif // PROTOBUF_PAYLOAD_IMPL_H_
