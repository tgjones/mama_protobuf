Protocol Buffers Payload for OpenMAMA
=====================================

This is an open-source demonstrator payload for OpenMAMA. It wraps 
the Google Protocol Buffers C++ API, providing the thinest possible 
C wrapper functions.

Disclaimer
----------
*This library has not been battle tested in a production environement, and is provided 
with absolutely no warrenty of any kind.* **Use at your own risk.**

Requirements
------------
 - [OpenMAMA](http://www.openmama.org) source distribution
 - A payload agnostic transport middleware (such as
 [NYSE Technologies Data Fabric](http://nysetechnologies.nyx.com/data-technology/data-fabric-6-0))
 - [Google Protocol Buffers](http://code.google.com/p/protobuf/) installed
 - [CMake](http://www.cmake.org) installed

Building
--------
 1. Ensure your OpenMAMA source has been built (to enable essential symlinks 
    to `wConfig.h` etc.)
 2. Add `MAMA_PAYLOAD_PROTOBUF = 'P'` to the `mamaPayloadType` enum in
    `<openmama_dir>/mama/c_cpp/src/c/mama/msg.h`
 3. Goto `<mama_pb_dir>/cmake` and type `cmake . -DOMAMA_SRC=<openmama_dir>`
 4. On Linux, type `make` (Windows is untested)
 5. The lib is output to `<mama_pb_dir>/build/lib`

Todo
----
 - Run up against a payload-agnostic middle (not Avis) and do some testing and
   the inevitable debuging as this code *with not work in its current state*.
 - Write a Google Test test suite and run against library.
 - Write an example application that dynamically loads`lib mama_pb.so`
 - Run valgrind against running instance to detect memory leaks/errors
 - Do performance testing and any performance tweaks
